Description: Use the C.UTF-8 locale for the unit tests
 The C.UTF-8 locale will always be available on recent Debian/Ubuntu releases.
Forwarded: no
Author: Peter Pentchev <roam@ringlet.net>
Last-Update: 2025-02-17

--- a/libarchive/test/test_archive_string_conversion.c
+++ b/libarchive/test/test_archive_string_conversion.c
@@ -260,7 +260,7 @@
 	int locale_is_utf8, wc_is_unicode;
 	int sconv_opt = SCONV_SET_OPT_NORMALIZATION_C;
 
-	locale_is_utf8 = (NULL != setlocale(LC_ALL, "en_US.UTF-8"));
+	locale_is_utf8 = (NULL != set_utf8_locale());
 	wc_is_unicode = is_wc_unicode();
 	/* If it doesn't exist, just warn and return. */
 	if (!locale_is_utf8 && !wc_is_unicode) {
@@ -471,7 +471,7 @@
 	int locale_is_utf8, wc_is_unicode;
 	int sconv_opt = SCONV_SET_OPT_NORMALIZATION_D;
 
-	locale_is_utf8 = (NULL != setlocale(LC_ALL, "en_US.UTF-8"));
+	locale_is_utf8 = (NULL != set_utf8_locale());
 	wc_is_unicode = is_wc_unicode();
 	/* If it doesn't exist, just warn and return. */
 	if (!locale_is_utf8 && !wc_is_unicode) {
@@ -713,7 +713,7 @@
 	struct archive *a;
 	struct archive_string_conv *sconv;
 
-	setlocale(LC_ALL, "en_US.UTF-8");
+	set_utf8_locale();
 
 	assert((a = archive_read_new()) != NULL);
 
@@ -823,7 +823,7 @@
 	struct archive_mstring mstr;
 	struct archive_string_conv *sc;
 
-	setlocale(LC_ALL, "en_US.UTF-8");
+	set_utf8_locale();
 
 	assert((a = archive_read_new()) != NULL);
 	memset(&mstr, 0, sizeof(mstr));
--- a/libarchive/test/test_entry.c
+++ b/libarchive/test/test_entry.c
@@ -914,7 +914,7 @@
 	/*
 	 * Exercise the character-conversion logic, if we can.
 	 */
-	if (NULL == setlocale(LC_ALL, "en_US.UTF-8")) {
+	if (NULL == set_utf8_locale()) {
 		skipping("Can't exercise charset-conversion logic without"
 			" a suitable locale.");
 	} else {
--- a/libarchive/test/test_gnutar_filename_encoding.c
+++ b/libarchive/test/test_gnutar_filename_encoding.c
@@ -32,7 +32,7 @@
 	char buff[4096];
 	size_t used;
 
-	if (NULL == setlocale(LC_ALL, "en_US.UTF-8")) {
+	if (NULL == set_utf8_locale()) {
 		skipping("en_US.UTF-8 locale not available on this system.");
 		return;
 	}
--- a/libarchive/test/test_pax_filename_encoding.c
+++ b/libarchive/test/test_pax_filename_encoding.c
@@ -106,7 +106,7 @@
 	 * en_US.UTF-8 seems to be commonly supported.
 	 */
 	/* If it doesn't exist, just warn and return. */
-	if (NULL == setlocale(LC_ALL, "en_US.UTF-8")) {
+	if (NULL == set_utf8_locale()) {
 		skipping("invalid encoding tests require a suitable locale;"
 		    " en_US.UTF-8 not available on this system");
 		return;
--- a/libarchive/test/test_read_format_cab_filename.c
+++ b/libarchive/test/test_read_format_cab_filename.c
@@ -92,8 +92,8 @@
 	/*
 	 * Read CAB filename in en_US.UTF-8 with "hdrcharset=CP932" option.
 	 */
-	if (NULL == setlocale(LC_ALL, "en_US.UTF-8")) {
-		skipping("en_US.UTF-8 locale not available on this system.");
+	if (NULL == set_utf8_locale()) {
+		skipping("A suitable UTF-8 locale is not available on this system.");
 		return;
 	}
 
--- a/libarchive/test/test_read_format_cpio_filename.c
+++ b/libarchive/test/test_read_format_cpio_filename.c
@@ -35,7 +35,7 @@
 	/*
 	 * Read eucJP filename in en_US.UTF-8 with "hdrcharset=eucJP" option.
 	 */
-	if (NULL == setlocale(LC_ALL, "en_US.UTF-8")) {
+	if (NULL == set_utf8_locale()) {
 		skipping("en_US.UTF-8 locale not available on this system.");
 		return;
 	}
@@ -148,7 +148,7 @@
 	/*
 	 * Read UTF-8 filename in en_US.UTF-8 without "hdrcharset=UTF-8" option.
 	 */
-	if (NULL == setlocale(LC_ALL, "en_US.UTF-8")) {
+	if (NULL == set_utf8_locale()) {
 		skipping("en_US.UTF-8 locale not available on this system.");
 		return;
 	}
@@ -249,7 +249,7 @@
 	/*
 	 * Read CP866 filename in en_US.UTF-8 with "hdrcharset=CP866" option.
 	 */
-	if (NULL == setlocale(LC_ALL, "en_US.UTF-8")) {
+	if (NULL == set_utf8_locale()) {
 		skipping("en_US.UTF-8 locale not available on this system.");
 		return;
 	}
@@ -354,7 +354,7 @@
 	/*
 	 * Read KOI8-R filename in en_US.UTF-8 with "hdrcharset=KOI8-R" option.
 	 */
-	if (NULL == setlocale(LC_ALL, "en_US.UTF-8")) {
+	if (NULL == set_utf8_locale()) {
 		skipping("en_US.UTF-8 locale not available on this system.");
 		return;
 	}
@@ -517,7 +517,7 @@
 	/*
 	 * Read UTF-8 filename in en_US.UTF-8 without "hdrcharset=UTF-8" option.
 	 */
-	if (NULL == setlocale(LC_ALL, "en_US.UTF-8")) {
+	if (NULL == set_utf8_locale()) {
 		skipping("en_US.UTF-8 locale not available on this system.");
 		return;
 	}
--- a/libarchive/test/test_read_format_gtar_filename.c
+++ b/libarchive/test/test_read_format_gtar_filename.c
@@ -35,7 +35,7 @@
 	/*
 	 * Read eucJP filename in en_US.UTF-8 with "hdrcharset=eucJP" option.
 	 */
-	if (NULL == setlocale(LC_ALL, "en_US.UTF-8")) {
+	if (NULL == set_utf8_locale()) {
 		skipping("en_US.UTF-8 locale not available on this system.");
 		return;
 	}
@@ -141,7 +141,7 @@
 	/*
 	 * Read CP866 filename in en_US.UTF-8 with "hdrcharset=CP866" option.
 	 */
-	if (NULL == setlocale(LC_ALL, "en_US.UTF-8")) {
+	if (NULL == set_utf8_locale()) {
 		skipping("en_US.UTF-8 locale not available on this system.");
 		return;
 	}
@@ -246,7 +246,7 @@
 	/*
 	 * Read KOI8-R filename in en_US.UTF-8 with "hdrcharset=KOI8-R" option.
 	 */
-	if (NULL == setlocale(LC_ALL, "en_US.UTF-8")) {
+	if (NULL == set_utf8_locale()) {
 		skipping("en_US.UTF-8 locale not available on this system.");
 		return;
 	}
--- a/libarchive/test/test_read_format_lha_filename.c
+++ b/libarchive/test/test_read_format_lha_filename.c
@@ -98,7 +98,7 @@
 	/*
 	 * Read LHA filename in en_US.UTF-8.
 	 */
-	if (NULL == setlocale(LC_ALL, "en_US.UTF-8")) {
+	if (NULL == set_utf8_locale()) {
 		skipping("en_US.UTF-8 locale not available on this system.");
 		return;
 	}
--- a/libarchive/test/test_read_format_lha_filename_utf16.c
+++ b/libarchive/test/test_read_format_lha_filename_utf16.c
@@ -35,7 +35,7 @@
 	/*
 	 * Read LHA filename in en_US.UTF-8.
 	 */
-	if (NULL == setlocale(LC_ALL, "en_US.UTF-8")) {
+	if (NULL == set_utf8_locale()) {
 		skipping("en_US.UTF-8 locale not available on this system.");
 		return;
 	}
--- a/libarchive/test/test_read_format_rar.c
+++ b/libarchive/test/test_read_format_rar.c
@@ -205,7 +205,7 @@
   struct archive_entry *ae;
   struct archive *a;
 
-  if (NULL == setlocale(LC_ALL, "en_US.UTF-8")) {
+  if (NULL == set_utf8_locale()) {
 	skipping("en_US.UTF-8 locale not available on this system.");
 	return;
   }
--- a/libarchive/test/test_read_format_tar_filename.c
+++ b/libarchive/test/test_read_format_tar_filename.c
@@ -164,7 +164,7 @@
 	 * Read filename in en_US.UTF-8 with "hdrcharset=KOI8-R" option.
 	 * We should correctly read two filenames.
 	 */
-	if (NULL == setlocale(LC_ALL, "en_US.UTF-8")) {
+	if (NULL == set_utf8_locale()) {
 		skipping("en_US.UTF-8 locale not available on this system.");
 		return;
 	}
--- a/libarchive/test/test_read_format_ustar_filename.c
+++ b/libarchive/test/test_read_format_ustar_filename.c
@@ -35,7 +35,7 @@
 	/*
 	 * Read eucJP filename in en_US.UTF-8 with "hdrcharset=eucJP" option.
 	 */
-	if (NULL == setlocale(LC_ALL, "en_US.UTF-8")) {
+	if (NULL == set_utf8_locale()) {
 		skipping("en_US.UTF-8 locale not available on this system.");
 		return;
 	}
@@ -144,7 +144,7 @@
 	/*
 	 * Read CP866 filename in en_US.UTF-8 with "hdrcharset=CP866" option.
 	 */
-	if (NULL == setlocale(LC_ALL, "en_US.UTF-8")) {
+	if (NULL == set_utf8_locale()) {
 		skipping("en_US.UTF-8 locale not available on this system.");
 		return;
 	}
@@ -255,7 +255,7 @@
 	/*
 	 * Read KOI8-R filename in en_US.UTF-8 with "hdrcharset=KOI8-R" option.
 	 */
-	if (NULL == setlocale(LC_ALL, "en_US.UTF-8")) {
+	if (NULL == set_utf8_locale()) {
 		skipping("en_US.UTF-8 locale not available on this system.");
 		return;
 	}
--- a/libarchive/test/test_read_format_zip_7075_utf8_paths.c
+++ b/libarchive/test/test_read_format_zip_7075_utf8_paths.c
@@ -74,7 +74,7 @@
 
 	extract_reference_file(refname);
 
-	if (NULL == setlocale(LC_ALL, "en_US.UTF-8")) {
+	if (NULL == set_utf8_locale()) {
 		skipping("en_US.UTF-8 locale not available on this system.");
 		return;
 	}
--- a/libarchive/test/test_read_format_zip_filename.c
+++ b/libarchive/test/test_read_format_zip_filename.c
@@ -95,7 +95,7 @@
 	/*
 	 * Read CP932 filename in en_US.UTF-8 with "hdrcharset=CP932" option.
 	 */
-	if (NULL == setlocale(LC_ALL, "en_US.UTF-8")) {
+	if (NULL == set_utf8_locale()) {
 		skipping("en_US.UTF-8 locale not available on this system.");
 		return;
 	}
@@ -250,7 +250,7 @@
 	 * because the file name in the sample file is UTF-8 and
 	 * Bit 11 of its general purpose bit flag is set.
 	 */
-	if (NULL == setlocale(LC_ALL, "en_US.UTF-8")) {
+	if (NULL == set_utf8_locale()) {
 		skipping("en_US.UTF-8 locale not available on this system.");
 		return;
 	}
@@ -399,7 +399,7 @@
 	/*
 	 * Read CP866 filename in en_US.UTF-8 with "hdrcharset=CP866" option.
 	 */
-	if (NULL == setlocale(LC_ALL, "en_US.UTF-8")) {
+	if (NULL == set_utf8_locale()) {
 		skipping("en_US.UTF-8 locale not available on this system.");
 		return;
 	}
@@ -512,7 +512,7 @@
 	/*
 	 * Read KOI8-R filename in en_US.UTF-8 with "hdrcharset=KOI8-R" option.
 	 */
-	if (NULL == setlocale(LC_ALL, "en_US.UTF-8")) {
+	if (NULL == set_utf8_locale()) {
 		skipping("en_US.UTF-8 locale not available on this system.");
 		return;
 	}
@@ -697,7 +697,7 @@
 	 * because the file name in the sample file is UTF-8 and
 	 * Bit 11 of its general purpose bit flag is set.
 	 */
-	if (NULL == setlocale(LC_ALL, "en_US.UTF-8")) {
+	if (NULL == set_utf8_locale()) {
 		skipping("en_US.UTF-8 locale not available on this system.");
 		return;
 	}
@@ -1127,7 +1127,7 @@
 	/*
 	 * Read filename in en_US.UTF-8 with "hdrcharset=KOI8-R" option.
 	 */
-	if (NULL == setlocale(LC_ALL, "en_US.UTF-8")) {
+	if (NULL == set_utf8_locale()) {
 		skipping("en_US.UTF-8 locale not available on this system.");
 		return;
 	}
--- a/libarchive/test/test_ustar_filename_encoding.c
+++ b/libarchive/test/test_ustar_filename_encoding.c
@@ -33,7 +33,7 @@
 	char buff[4096];
 	size_t used;
 
-	if (NULL == setlocale(LC_ALL, "en_US.UTF-8")) {
+	if (NULL == set_utf8_locale()) {
 		skipping("en_US.UTF-8 locale not available on this system.");
 		return;
 	}
--- a/libarchive/test/test_zip_filename_encoding.c
+++ b/libarchive/test/test_zip_filename_encoding.c
@@ -33,7 +33,7 @@
 	char buff[4096];
 	size_t used;
 
-	if (NULL == setlocale(LC_ALL, "en_US.UTF-8")) {
+	if (NULL == set_utf8_locale()) {
 		skipping("en_US.UTF-8 locale not available on this system.");
 		return;
 	}
--- a/test_utils/test_utils.h
+++ b/test_utils/test_utils.h
@@ -33,4 +33,7 @@
 /* Fill a buffer with pseudorandom data */
 void fill_with_pseudorandom_data(void* buffer, size_t size);
 
+/* Try to switch to a UTF-8-capable locale, return its full name. */
+const char *set_utf8_locale(void);
+
 #endif /* TEST_UTILS_H */
--- a/unzip/test/test_I.c
+++ b/unzip/test/test_I.c
@@ -25,30 +25,27 @@
  */
 #include "test.h"
 
-#ifdef HAVE_LOCALE_H
-#include <locale.h>
-#endif
-
 /* Test I arg - file name encoding */
 DEFINE_TEST(test_I)
 {
+#if HAVE_SETENV
 	const char *reffile = "test_I.zip";
 	int r;
 
-#if HAVE_SETLOCALE
-	if (NULL == setlocale(LC_ALL, "en_US.UTF-8")) {
-		skipping("en_US.UTF-8 locale not available on this system.");
+	const char *utf8_locale = set_utf8_locale();
+	if (NULL == utf8_locale) {
+		skipping("No UTF-8-capable locale available on this system.");
 		return;
 	}
-#else
-	skipping("setlocale() not available on this system.");
-#endif
 
 	extract_reference_file(reffile);
-	r = systemf("env LC_ALL=%s %s -I UTF-8 %s >test.out 2>test.err", "en_US.UTF-8", testprog, reffile);
+	r = systemf("env LC_ALL=%s %s -I UTF-8 %s >test.out 2>test.err", utf8_locale, testprog, reffile);
 	assertEqualInt(0, r);
 	assertNonEmptyFile("test.out");
 	assertEmptyFile("test.err");
 
 	assertTextFileContents("Hello, World!\n", "Γειά σου Κόσμε.txt");
+#else
+	skipping("No setenv()");
+#endif
 }
--- a/test_utils/test_main.c
+++ b/test_utils/test_main.c
@@ -84,6 +84,12 @@
 #if HAVE_MEMBERSHIP_H
 #include <membership.h>
 #endif
+#if HAVE_LOCALE_H
+#include <locale.h>
+#endif
+#if HAVE_LANGINFO_H
+#include <langinfo.h>
+#endif
 
 /*
  *
@@ -565,6 +571,32 @@
 	++skips;
 }
 
+static const char *UTF8_LOCALES[] = {"C.UTF-8", "en_US.UTF-8"};
+
+#define UTF8_LOCALES_COUNT (sizeof(UTF8_LOCALES) / sizeof(UTF8_LOCALES[0]))
+
+const char *
+set_utf8_locale(void)
+{
+#if HAVE_SETLOCALE
+	logprintf("set_utf8_locale(): looking for a suitable UTF-8-capable locale\n");
+	logprintf("- codeset: %s\n", nl_langinfo(CODESET));
+	for (size_t loc_idx = 0; loc_idx < UTF8_LOCALES_COUNT; loc_idx++) {
+		const char * const name = UTF8_LOCALES[loc_idx];
+		logprintf("- trying %s\n", name);
+		if (NULL != setlocale(LC_ALL, name)) {
+			logprintf("  - using %s; codeset: %s\n", name, nl_langinfo(CODESET));
+			return name;
+		}
+	}
+	logprintf("- none of the locales found, skip the test?\n");
+	return NULL;
+#else
+	logprintf("set_utf8_locale(): no setlocale(3), not even trying\n");
+	return NULL;
+#endif
+}
+
 /*
  *
  * ASSERTIONS
